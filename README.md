# gym-crm-system-spring
The Gym CRM System is a Spring-based application designed to manage gym memberships, trainers, and training sessions efficiently. 
It provides a set of services for creating, updating, and deleting profiles of trainees and trainers, as well as managing training sessions using Spring Core.

#### Gym CRM System API exploration using Swagger UI
Explore the Gym CRM System API documentation using Swagger UI. Access it at:
[Gym CRM System](http://localhost:8080/swagger-ui/index.html#/).