package com.epam.gymcrmsystem.api;

import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainee.TraineeGetResponseDTO;
import com.epam.gymcrmsystem.dto.trainee.TraineeRequestDTO;
import com.epam.gymcrmsystem.dto.trainee.TraineeTrainersUpdateRequestDTO;
import com.epam.gymcrmsystem.dto.trainee.TraineeUpdateRequestDTO;
import com.epam.gymcrmsystem.dto.trainer.TrainerDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan on 13.06.2024
 */

public interface TraineeApi {

    @Operation(
            summary = "Create a new trainee",
            description = "Creates a new trainee in the system."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "201",
                    description = "Successfully created the trainee.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PostMapping
    ResponseEntity<?> createTrainee(@Valid @RequestBody TraineeRequestDTO traineeRequestDTO, BindingResult bindingResult);

    @Operation(
            summary = "Change password for a user",
            description = "Changes the password for a user."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully changed the password.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PostMapping("/change-password")
    ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, BindingResult bindingResult);

    @Operation(
            summary = "Update a trainee",
            description = "Updates a trainee's details."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully updated the trainee.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PutMapping
    ResponseEntity<?> updateTrainee(@Valid @RequestBody TraineeUpdateRequestDTO traineeUpdateRequestDTO, BindingResult bindingResult);

    @Operation(
            summary = "Activate or deactivate a trainee",
            description = "Activates or deactivates a trainee based on the provided details."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully activated/deactivated the trainee.",
                    content = @Content(
                            schema = @Schema(implementation = String.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PatchMapping("/activate-deactivate")
    ResponseEntity<?> activateDeactivateTrainee(@Valid @RequestBody UserActivationDTO userActivationDTO, BindingResult bindingResult);

    @Operation(
            summary = "Delete a trainee",
            description = "Deletes a trainee by their username."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully deleted the trainee.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @DeleteMapping("/{username}")
    ResponseEntity<?> deleteTrainee(@PathVariable String username);

    @Operation(
            summary = "Retrieve a trainee by username",
            description = "Retrieves a trainee's details by their username."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully retrieved the trainee.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "Not Found. The trainee does not exist.",
                    description = "Not Found.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @GetMapping("/username/{username}")
    ResponseEntity<TraineeGetResponseDTO> getTrainee(@PathVariable String username);

    @Operation(
            summary = "Search trainings for a trainee",
            description = "Searches for trainings for a trainee based on various criteria."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully found the trainings.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. No trainings match the criteria.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @GetMapping("/{username}/trainings/search")
    ResponseEntity<?> getTraineeTrainings(
            @PathVariable String username,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
            @RequestParam(required = false) String trainerName,
            @RequestParam(required = false) String trainingType);

    @Operation(
            summary = "Get unassigned trainers by trainee username",
            description = "Retrieves a list of trainers not assigned to the trainee with the given username."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully retrieved the unassigned trainers.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. No unassigned trainers found.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @GetMapping("/{username}/unassigned-trainers")
    ResponseEntity<List<TrainerDTO>> getUnassignedTrainersByTraineeUsername(@PathVariable String username);

    @Operation(
            summary = "Update trainers for a trainee",
            description = "Updates the trainers assigned to a trainee."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully updated the trainers.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PutMapping("/update-trainers")
    ResponseEntity<?> updateTraineeTrainers(@Valid @RequestBody TraineeTrainersUpdateRequestDTO traineeTrainersUpdateRequestDTO, BindingResult bindingResult);
}
