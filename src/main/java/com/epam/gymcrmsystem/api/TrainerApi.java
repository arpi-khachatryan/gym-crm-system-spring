package com.epam.gymcrmsystem.api;

import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainer.TrainerRequestDTO;
import com.epam.gymcrmsystem.dto.trainer.TrainerUpdateRequestDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

/**
 * @author Arpi Khachatryan on 13.06.2024
 */

public interface TrainerApi {

    @Operation(
            summary = "Create a new trainer",
            description = "Creates a new trainer in the system."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "201",
                    description = "Successfully created the trainer.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PostMapping
    ResponseEntity<?> createTrainer(@Valid @RequestBody TrainerRequestDTO trainerRequestDTO, BindingResult bindingResult);

    @Operation(
            summary = "Change password for a user",
            description = "Changes the password for a user."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully changed the password.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PostMapping("/change-password")
    ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, BindingResult bindingResult);

    @Operation(
            summary = "Update a trainer",
            description = "Updates a trainer's details."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully updated the trainer.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PutMapping
    ResponseEntity<?> updateTrainer(@Valid @RequestBody TrainerUpdateRequestDTO trainerUpdateRequestDTO, BindingResult bindingResult);

    @Operation(
            summary = "Activate or deactivate a trainer",
            description = "Activates or deactivates a trainer based on the provided details."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully activated/deactivated the trainer.",
                    content = @Content(
                            schema = @Schema(implementation = String.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @PatchMapping("/activate-deactivate")
    ResponseEntity<?> activateDeactivateTrainer(@Valid @RequestBody UserActivationDTO userActivationDTO, BindingResult bindingResult);

    @Operation(
            summary = "Retrieve a trainer by username",
            description = "Retrieves a trainer's details by their username."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully retrieved the trainer.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. The trainer does not exist.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @GetMapping("/username/{username}")
    ResponseEntity<?> getTrainer(@PathVariable String username);

    @Operation(
            summary = "Search trainings for a trainer",
            description = "Searches for trainings for a trainer based on various criteria."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully found the trainings.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. No trainings match the criteria.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    @GetMapping("/{username}/trainings/search")
    ResponseEntity<?> getTrainerTrainings(
            @PathVariable String username,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
            @RequestParam(required = false) String traineeName);
}
