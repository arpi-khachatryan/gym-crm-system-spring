package com.epam.gymcrmsystem.api.controller;

import com.epam.gymcrmsystem.api.TrainerApi;
import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainer.*;
import com.epam.gymcrmsystem.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystem.service.TrainerService;
import com.epam.gymcrmsystem.util.LoggingUtils;
import com.epam.gymcrmsystem.validation.ValidationErrorMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/api/v1/trainers")
public class TrainerController implements TrainerApi {

    private final TrainerService trainerService;

    @Autowired
    public TrainerController(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @Override
    @PostMapping
    public ResponseEntity<?> createTrainer(@Valid @RequestBody TrainerRequestDTO trainerRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to create new trainer: {}", transactionId, trainerRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: POST /createTrainer, Request body: {}", transactionId, trainerRequestDTO);
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        TrainerResponseDTO createdTrainer = trainerService.createTrainer(trainerRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainer created successfully: {}", transactionId, createdTrainer);
        log.debug("Transaction ID: {}. Response: HTTP 201 Created, Created trainer: {}", transactionId, createdTrainer);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdTrainer);
    }

    @Override
    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to change password for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
        log.debug("Transaction ID: {}. Endpoint called: POST /change-password, Request body: {}", transactionId, passwordChangeDTO);
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        trainerService.changePassword(passwordChangeDTO, transactionId);
        log.info("Transaction ID: {}. Password changed successfully for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.ok().build();
    }

    @Override
    @PutMapping
    public ResponseEntity<?> updateTrainer(@Valid @RequestBody TrainerUpdateRequestDTO trainerUpdateRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to update trainer: {}", transactionId, trainerUpdateRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: PUT /updateTrainer, Request body: {}", transactionId, trainerUpdateRequestDTO);
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        TrainerUpdateResponseDTO updatedTrainer = trainerService.updateTrainer(trainerUpdateRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainer updated successfully: {}", transactionId, updatedTrainer);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.status(HttpStatus.OK).body(updatedTrainer);
    }

    @Override
    @PatchMapping("/activate-deactivate")
    public ResponseEntity<?> activateDeactivateTrainer(@Valid @RequestBody UserActivationDTO userActivationDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to activate/deactivate trainer with username: {}", transactionId, userActivationDTO.getUsername());
        log.debug("Transaction ID: {}. Endpoint called: PATCH /activate-deactivate, Request body: {}", transactionId, userActivationDTO);
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        trainerService.activateDeactivateTrainer(userActivationDTO, transactionId);
        log.info("Transaction ID: {}. Trainer with username {} activated/deactivated successfully.", transactionId, userActivationDTO.getUsername());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.ok().body("Trainer with username " + userActivationDTO.getUsername() + " activated/deactivated successfully.");
    }

    @Override
    @GetMapping("/username/{username}")
    public ResponseEntity<?> getTrainer(@PathVariable String username) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Retrieving trainer with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET /username/{}", transactionId, username);
        TrainerGetResponseDTO trainer = trainerService.getTrainer(username, transactionId);
        log.info("Transaction ID: {}. Retrieved trainer: {}", transactionId, trainer);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.status(HttpStatus.OK).body(trainer);
    }

    @Override
    @GetMapping("/{username}/trainings/search")
    public ResponseEntity<?> getTrainerTrainings(
            @PathVariable String username,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
            @RequestParam(required = false) String traineeName) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Searching trainings for trainer with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET /{}/trainings/search", transactionId, username);
        log.debug("Transaction ID: {}. Query parameters - periodFrom: {}, periodTo: {}, traineeName: {}", transactionId, periodFrom, periodTo, traineeName);
        List<TrainerTrainingResponseDTO> trainings = trainerService.getTrainerTrainingsByCriteria(username, periodFrom, periodTo, traineeName, transactionId);
        log.info("Transaction ID: {}. Found {} trainings for trainer with username: {}", transactionId, trainings.size(), username);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.status(HttpStatus.OK).body(trainings);
    }
}