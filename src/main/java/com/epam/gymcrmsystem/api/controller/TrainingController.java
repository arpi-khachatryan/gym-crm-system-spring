package com.epam.gymcrmsystem.api.controller;

import com.epam.gymcrmsystem.api.TrainingApi;
import com.epam.gymcrmsystem.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystem.service.TrainingService;
import com.epam.gymcrmsystem.util.LoggingUtils;
import com.epam.gymcrmsystem.validation.ValidationErrorMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/api/v1/trainings")
public class TrainingController implements TrainingApi {

    private final TrainingService trainingService;

    @Autowired
    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @Override
    @PostMapping
    public ResponseEntity<?> createTraining(@Valid @RequestBody TrainingRequestDTO trainingRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Creating new training: {}", transactionId, trainingRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: POST /", transactionId);
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        trainingService.createTraining(trainingRequestDTO, transactionId);
        log.info("Transaction ID: {}. Training created successfully", transactionId);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}