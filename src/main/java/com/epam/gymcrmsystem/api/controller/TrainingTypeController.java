package com.epam.gymcrmsystem.api.controller;

import com.epam.gymcrmsystem.api.TrainingTypeApi;
import com.epam.gymcrmsystem.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystem.service.TrainingTypeService;
import com.epam.gymcrmsystem.util.LoggingUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Arpi Khachatryan on 08.06.2024
 */

@Slf4j
@RestController
@RequestMapping("/api/v1/trainingTypes")
public class TrainingTypeController implements TrainingTypeApi {

    private final TrainingTypeService trainingTypeService;

    @Autowired
    public TrainingTypeController(TrainingTypeService trainingTypeService) {
        this.trainingTypeService = trainingTypeService;
    }

    @Override
    @GetMapping
    public ResponseEntity<?> getTrainingTypes() {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Fetching all training types", transactionId);
        log.debug("Transaction ID: {}. Endpoint called: GET /", transactionId);
        List<TrainingTypeResponseDTO> trainingTypes = trainingTypeService.getTrainingTypes(transactionId);
        log.info("Transaction ID: {}. Fetched {} training types successfully", transactionId, trainingTypes.size());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        return ResponseEntity.status(HttpStatus.OK).body(trainingTypes);
    }
}