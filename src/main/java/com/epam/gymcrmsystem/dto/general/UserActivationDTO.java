package com.epam.gymcrmsystem.dto.general;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author Arpi Khachatryan on 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserActivationDTO {
    @NotBlank
    private String username;
    @NotNull
    private Boolean isActive;
}