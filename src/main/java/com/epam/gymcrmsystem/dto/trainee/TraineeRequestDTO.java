package com.epam.gymcrmsystem.dto.trainee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.Set;

/**
 * @author Arpi Khachatryan on 08.06.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeRequestDTO {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private Date dateOfBirth;
    private String address;
    private Long trainerId;
    private Set<Long> trainingIds;
}