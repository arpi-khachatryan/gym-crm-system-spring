package com.epam.gymcrmsystem.dto.trainee;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan on 08.06.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeResponseDTO {
    private String username;
    private String password;
}
