package com.epam.gymcrmsystem.dto.trainee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Arpi Khachatryan on 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeUpdateRequestDTO {
    @NotBlank
    private String username;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private Date dateOfBirth;
    private String address;
    @NotNull
    private Boolean isActive;
}