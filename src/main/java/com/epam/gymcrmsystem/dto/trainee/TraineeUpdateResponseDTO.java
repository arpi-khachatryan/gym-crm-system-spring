package com.epam.gymcrmsystem.dto.trainee;

import com.epam.gymcrmsystem.dto.trainer.TrainerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan on 08.06.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeUpdateResponseDTO {
    private String username;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String address;
    private boolean isActive;
    private List<TrainerDTO> trainers;
}
