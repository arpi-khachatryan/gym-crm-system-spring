package com.epam.gymcrmsystem.dto.trainer;

import com.epam.gymcrmsystem.dto.trainee.TraineeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Arpi Khachatryan on 08.06.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerUpdateResponseDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String specialization;
    private boolean isActive;
    private List<TraineeDTO> trainees;
}
