package com.epam.gymcrmsystem.dto.training;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @author Arpi Khachatryan on 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerTrainingSearchCriteriaDTO {
    @NotNull
    private Date fromDate;
    @NotNull
    private Date toDate;
    @NotBlank
    private String traineeName;
}