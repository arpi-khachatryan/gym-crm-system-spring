package com.epam.gymcrmsystem.dto.trainingType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 *  Author: Arpi Khachatryan on 09.06.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainingTypeResponseDTO {
    private Long trainingTypeId;
    private String trainingTypeName;
}
