package com.epam.gymcrmsystem.exception;

/**
 * @author Arpi Khachatryan on 10.06.2024
 */

public class ResourceCreationException extends RuntimeException {
    public ResourceCreationException(String message) {
        super(message);
    }

    public ResourceCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}

