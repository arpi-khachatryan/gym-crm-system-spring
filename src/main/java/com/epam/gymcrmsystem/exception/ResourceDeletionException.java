package com.epam.gymcrmsystem.exception;

/**
 * @author Arpi Khachatryan on 10.06.2024
 */

public class ResourceDeletionException extends RuntimeException {
    public ResourceDeletionException(String message) {
        super(message);
    }

    public ResourceDeletionException(String message, Throwable cause) {
        super(message, cause);
    }
}
