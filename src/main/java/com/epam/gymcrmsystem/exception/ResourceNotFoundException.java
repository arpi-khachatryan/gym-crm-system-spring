package com.epam.gymcrmsystem.exception;

/**
 * @author Arpi Khachatryan on 10.06.2024
 */

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
