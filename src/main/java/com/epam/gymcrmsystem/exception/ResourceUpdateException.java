package com.epam.gymcrmsystem.exception;

/**
 * @author Arpi Khachatryan on 10.06.2024
 */

public class ResourceUpdateException extends RuntimeException {
    public ResourceUpdateException(String message) {
        super(message);
    }

    public ResourceUpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
