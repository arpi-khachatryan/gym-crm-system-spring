package com.epam.gymcrmsystem.mapper;

import com.epam.gymcrmsystem.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystem.entity.TrainingType;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author Arpi Khachatryan on 09.06.2024
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TrainingTypeMapper {
    List<TrainingTypeResponseDTO> trainingTypeToTrainingTypeDTO(List<TrainingType> trainingTypes);
}