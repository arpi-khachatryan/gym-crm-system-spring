package com.epam.gymcrmsystem.repository;

import com.epam.gymcrmsystem.entity.TrainingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan on 28.05.2024
 */

@Repository
public interface TrainingTypeRepository extends JpaRepository<TrainingType, Long> {
    TrainingType findByName(String trainingTypeName);

    boolean existsByName(String name);
}