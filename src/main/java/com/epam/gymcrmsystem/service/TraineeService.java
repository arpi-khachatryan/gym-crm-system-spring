package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainee.*;
import com.epam.gymcrmsystem.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystem.dto.training.TraineeTrainingResponseDTO;

import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TraineeService {
    TraineeResponseDTO createTrainee(TraineeRequestDTO traineeRequestDTO, String transactionId);

    TraineeUpdateResponseDTO updateTrainee(TraineeUpdateRequestDTO traineeUpdateRequestDTO, String transactionId);

    void deleteTrainee(String username, String transactionId);

    TraineeGetResponseDTO getTrainee(String username, String transactionId);

    void changePassword(PasswordChangeDTO passwordChangeDTO, String transactionId);

    void activateDeactivateTrainee(UserActivationDTO userActivationDTO, String transactionId);

    List<TraineeTrainingResponseDTO> getTraineeTrainingsByCriteria(String username, Date periodFrom, Date periodTo, String trainerName, String trainingTyp, String transactionId);

    List<TrainerDTO> getUnassignedTrainersByTraineeUsername(String username, String transactionId);

    List<TrainerDTO> updateTraineeTrainers(TraineeTrainersUpdateRequestDTO traineeTrainersUpdateRequestDTO, String transactionId);
}