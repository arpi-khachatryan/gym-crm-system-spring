package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainer.*;
import com.epam.gymcrmsystem.dto.training.TrainerTrainingResponseDTO;

import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TrainerService {
    TrainerResponseDTO createTrainer(TrainerRequestDTO trainerRequestDTO, String transactionId);

    TrainerUpdateResponseDTO updateTrainer(TrainerUpdateRequestDTO trainerUpdateRequestDTO, String transactionId);

    TrainerGetResponseDTO getTrainer(String username, String transactionId);

    void changePassword(PasswordChangeDTO passwordChangeDTO, String transactionId);

    void activateDeactivateTrainer(UserActivationDTO userActivationDTO, String transactionId);

    List<TrainerTrainingResponseDTO> getTrainerTrainingsByCriteria(String username, Date periodFrom, Date periodTo, String traineeName, String transactionId);
}