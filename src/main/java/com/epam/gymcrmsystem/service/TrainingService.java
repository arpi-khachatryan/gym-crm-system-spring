package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.dto.training.TrainingRequestDTO;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TrainingService {
    void createTraining(TrainingRequestDTO trainingRequestDTO,String transactionId);
}