package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.dto.trainingType.TrainingTypeResponseDTO;

import java.util.List;

/**
 * @author Arpi Khachatryan on 08.06.2024
 */

public interface TrainingTypeService {
    List<TrainingTypeResponseDTO> getTrainingTypes(String transactionId);
}