package com.epam.gymcrmsystem.util;

import java.util.UUID;

/**
 * @author Arpi Khachatryan on 09.06.2024
 */

public class LoggingUtils {
    public static String generateTransactionId() {
        return UUID.randomUUID().toString();
    }
}
