package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.api.controller.TraineeController;
import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainee.*;
import com.epam.gymcrmsystem.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystem.dto.trainer.TrainerUpdateDTO;
import com.epam.gymcrmsystem.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystem.dto.training.TraineeTrainingSearchCriteriaDTO;
import com.epam.gymcrmsystem.service.TraineeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.gymcrmsystem.parametrs.MockData.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan on 04.06.2024
 */

public class TraineeControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TraineeService traineeService;

    @InjectMocks
    private TraineeController traineeController;

    @BeforeEach
    void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        createTestDataSource();
        mockMvc = MockMvcBuilders.standaloneSetup(traineeController).build();
    }

    private DataSource createTestDataSource() throws SQLException {
        DataSource dataSource = new org.h2.jdbcx.JdbcDataSource();
        ((org.h2.jdbcx.JdbcDataSource) dataSource).setURL("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1");
        try (Connection connection = dataSource.getConnection(); Statement stmt = connection.createStatement()) {
            stmt.execute("DROP TABLE IF EXISTS trainer_trainee");
            stmt.execute("DROP TABLE IF EXISTS trainers");
            stmt.execute("DROP TABLE IF EXISTS trainees");
            stmt.execute("DROP TABLE IF EXISTS training_types");
            stmt.execute("DROP TABLE IF EXISTS trainings");
            stmt.execute("CREATE TABLE trainees (id INT AUTO_INCREMENT PRIMARY KEY, first_name VARCHAR(255), last_name VARCHAR(255), " +
                    "username VARCHAR(255) UNIQUE NOT NULL, password VARCHAR(255) NOT NULL, date_of_birth DATE,is_active BOOLEAN NOT NULL, address VARCHAR(255))");
        }
        return dataSource;
    }

    @AfterEach
    void tearDown() {
        reset(traineeService);
    }

    @Test
    public void testCreateTrainee() throws Exception {
        TraineeRequestDTO requestDTO = getTraineeRequestDTO();
        TraineeResponseDTO createdTrainee = getTraineeResponseDTO();

        when(traineeService.createTrainee(any(TraineeRequestDTO.class), anyString())).thenReturn(createdTrainee);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/trainees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isCreated())
                .andExpect(jsonPath("$.username").value("firstName.lastName"))
                .andExpect(jsonPath("$.password").value("password1234"));
    }

    @Test
    public void testChangePassword() throws Exception {
        PasswordChangeDTO requestDTO = getPasswordChangeDTO();

        doNothing().when(traineeService).changePassword(any(PasswordChangeDTO.class), anyString());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/trainees/change-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testUpdateTrainee() throws Exception {
        TraineeUpdateRequestDTO requestDTO = getTraineeUpdateRequestDTO();
        TraineeUpdateResponseDTO updatedTrainee = getTraineeUpdateResponseDTO();

        when(traineeService.updateTrainee(any(TraineeUpdateRequestDTO.class), anyString())).thenReturn(updatedTrainee);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/trainees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("firstName.lastName"))
                .andExpect(jsonPath("$.firstName").value("firstName"))
                .andExpect(jsonPath("$.lastName").value("lastName"))
                .andExpect(jsonPath("$.address").value("address"));
    }

    @Test
    public void testActivateDeactivateTrainee() throws Exception {
        UserActivationDTO requestDTO = getUserActivationDTO();

        doNothing().when(traineeService).activateDeactivateTrainee(any(UserActivationDTO.class), anyString());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/trainees/activate-deactivate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testDeleteTrainee() throws Exception {
        doNothing().when(traineeService).deleteTrainee(any(String.class), any(String.class));

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/trainees/{username}", "firstName.lastName")
                .contentType(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testGetTrainee() throws Exception {
        TraineeGetResponseDTO responseDTO = getTraineeGetResponseDTO();

        when(traineeService.getTrainee(any(String.class), any(String.class))).thenReturn(responseDTO);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/trainees/username/{username}", "firstName.lastName")
                .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(responseDTO.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(responseDTO.getLastName()))
                .andExpect(jsonPath("$.dateOfBirth").value(responseDTO.getDateOfBirth()))
                .andExpect(jsonPath("$.address").value(responseDTO.getAddress()));
    }

    @Test
    public void testGetTraineeTrainings() throws Exception {
        TraineeTrainingSearchCriteriaDTO searchCriteriaDTO = getTraineeTrainingSearchCriteriaDTO();
        TraineeTrainingResponseDTO training = getTraineeTrainingResponseDTO();
        List<TraineeTrainingResponseDTO> trainings = Collections.singletonList(training);

        when(traineeService.getTraineeTrainingsByCriteria("firstName.lastName",
                searchCriteriaDTO.getFromDate(),
                searchCriteriaDTO.getToDate(),
                searchCriteriaDTO.getTrainerName(),
                searchCriteriaDTO.getTrainingTypeName(), "transactionId")).thenReturn(trainings);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/trainees/{username}/trainings/search", "firstName.lastName")
                .param("periodFrom", new SimpleDateFormat("yyyy-MM-dd").format(searchCriteriaDTO.getFromDate()))
                .param("periodTo", new SimpleDateFormat("yyyy-MM-dd").format(searchCriteriaDTO.getToDate()))
                .param("trainerName", searchCriteriaDTO.getTrainerName())
                .param("trainingType", searchCriteriaDTO.getTrainingTypeName())
                .contentType(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testGetUnassignedTrainers() throws Exception {
        List<TrainerDTO> unassignedTrainers = Collections.singletonList(getTrainerDTO());
        when(traineeService.getUnassignedTrainersByTraineeUsername("firstName.lastName", "transactionId")).thenReturn(unassignedTrainers);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/trainees/{username}/unassigned-trainers", "firstName.lastName"));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testUpdateTraineeTrainers() throws Exception {
        List<Long> trainerIds = Arrays.asList(1L, 2L, 3L);
        TraineeTrainersUpdateRequestDTO requestDTO = TraineeTrainersUpdateRequestDTO.builder()
                .username("firstName.lastName")
                .trainerUpdateDTOList(
                        trainerIds.stream()
                                .map(trainerId -> TrainerUpdateDTO.builder().username("firstName.lastName").build())
                                .collect(Collectors.toList())
                )
                .build();

        when(traineeService.updateTraineeTrainers(eq(requestDTO), anyString())).thenReturn(List.of(getTrainerDTO()));

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/trainees/update-trainers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isOk());
    }

    private String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}