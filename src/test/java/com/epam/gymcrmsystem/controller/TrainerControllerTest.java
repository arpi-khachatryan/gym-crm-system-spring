package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.api.controller.TrainerController;
import com.epam.gymcrmsystem.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystem.dto.general.UserActivationDTO;
import com.epam.gymcrmsystem.dto.trainer.*;
import com.epam.gymcrmsystem.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystem.service.TrainerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.*;

import static com.epam.gymcrmsystem.parametrs.MockData.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan on 04.06.2024
 */

public class TrainerControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TrainerService trainerService;

    @InjectMocks
    private TrainerController trainerController;

    @BeforeEach
    void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        createTestDataSource();
        mockMvc = MockMvcBuilders.standaloneSetup(trainerController).build();
    }

    private DataSource createTestDataSource() throws SQLException {
        DataSource dataSource = new org.h2.jdbcx.JdbcDataSource();
        ((org.h2.jdbcx.JdbcDataSource) dataSource).setURL("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1");
        try (Connection connection = dataSource.getConnection(); Statement stmt = connection.createStatement()) {
            stmt.execute("DROP TABLE IF EXISTS trainer_trainee");
            stmt.execute("DROP TABLE IF EXISTS trainees");
            stmt.execute("DROP TABLE IF EXISTS trainers");
            stmt.execute("DROP TABLE IF EXISTS training_types");
            stmt.execute("DROP TABLE IF EXISTS trainings");
            stmt.execute("CREATE TABLE trainers (id INT AUTO_INCREMENT PRIMARY KEY, first_name VARCHAR(255), last_name VARCHAR(255),username VARCHAR(255) UNIQUE NOT NULL, password VARCHAR(255) NOT NULL, " +
                    "date_of_birth DATE,is_active BOOLEAN NOT NULL, address VARCHAR(255))");
        }
        return dataSource;
    }

    @AfterEach
    void tearDown() {
        reset(trainerService);
    }

    @Test
    public void testCreateTrainer() throws Exception {
        TrainerRequestDTO requestDTO = getTrainerRequestDTO();
        requestDTO.setTraineeIds(new HashSet<>(Arrays.asList(1L, 2L, 3L)));
        TrainerResponseDTO createdTrainerResponse = getTrainerResponseDTO();

        when(trainerService.createTrainer(any(TrainerRequestDTO.class), anyString())).thenReturn(createdTrainerResponse);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/trainers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(createdTrainerResponse.getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").value(createdTrainerResponse.getPassword()));
    }

    @Test
    public void testChangePassword() throws Exception {
        PasswordChangeDTO requestDTO = getPasswordChangeDTO();

        doNothing().when(trainerService).changePassword(any(PasswordChangeDTO.class), anyString());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/trainers/change-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testUpdateTrainer() throws Exception {
        TrainerUpdateRequestDTO trainerUpdateRequestDTO = getTrainerUpdateRequestDTO();
        TrainerUpdateResponseDTO updatedTrainer = getTrainerUpdateResponseDTO();

        when(trainerService.updateTrainer(any(TrainerUpdateRequestDTO.class), anyString())).thenReturn(updatedTrainer);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/trainers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(trainerUpdateRequestDTO)));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testActivateDeactivateTrainerSuccess() throws Exception {
        UserActivationDTO requestDTO = getUserActivationDTO();

        doNothing().when(trainerService).activateDeactivateTrainer(any(UserActivationDTO.class), anyString());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/trainers/activate-deactivate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDTO)));

        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testGetTrainer() throws Exception {
        TrainerGetResponseDTO trainerGetResponseDTO = getTrainerGetResponseDTO();

        when(trainerService.getTrainer(any(String.class), anyString())).thenReturn(trainerGetResponseDTO);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/trainers/username/{username}", "firstName.lastName")
                .contentType(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void testGetTrainerTrainings() throws Exception {
        List<TrainerTrainingResponseDTO> trainerTrainingResponseDTO = new ArrayList<>();
        TrainerTrainingResponseDTO responseDTO = getTrainerTrainingResponseDTO();
        trainerTrainingResponseDTO.add(responseDTO);

        when(trainerService.getTrainerTrainingsByCriteria(any(String.class), any(Date.class), any(Date.class), any(String.class), any(String.class)))
                .thenReturn(trainerTrainingResponseDTO);
        LocalDate fromDate = LocalDate.now().minusDays(5);
        LocalDate toDate = LocalDate.now();

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/trainers/{username}/trainings/search", "firstName.lastName")
                .param("periodFrom", fromDate.toString())
                .param("periodTo", toDate.toString())
                .param("traineeName", "traineeName")
                .contentType(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk());
    }

    private String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
