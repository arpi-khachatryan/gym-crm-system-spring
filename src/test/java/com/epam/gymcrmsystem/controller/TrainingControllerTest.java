package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.api.controller.TrainingController;
import com.epam.gymcrmsystem.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystem.service.TrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static com.epam.gymcrmsystem.parametrs.MockData.getTrainingRequestDTO;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan on 04.06.2024
 */

public class TrainingControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TrainingService trainingService;

    @InjectMocks
    private TrainingController trainingController;

    @BeforeEach
    void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        createTestDataSource();
        mockMvc = MockMvcBuilders.standaloneSetup(trainingController).build();
    }

    private DataSource createTestDataSource() throws SQLException {
        DataSource dataSource = new org.h2.jdbcx.JdbcDataSource();
        ((org.h2.jdbcx.JdbcDataSource) dataSource).setURL("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1");
        try (Connection connection = dataSource.getConnection(); Statement stmt = connection.createStatement()) {
            stmt.execute("DROP TABLE IF EXISTS trainers");
            stmt.execute("DROP TABLE IF EXISTS trainees");
            stmt.execute("DROP TABLE IF EXISTS training_types");
            stmt.execute("DROP TABLE IF EXISTS trainings");
            stmt.execute("CREATE TABLE trainings (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL, training_date DATE NOT NULL, " +
                    "duration INT NOT NULL, trainee_id INT NOT NULL, trainer_id INT NOT NULL, training_type_id INT NOT NULL)");
        }
        return dataSource;
    }

    @AfterEach
    void tearDown() {
        reset(trainingService);
    }

    @Test
    public void testCreateTraining() throws Exception {
        TrainingRequestDTO trainingRequestDTO = getTrainingRequestDTO();

        doNothing().when(trainingService).createTraining(trainingRequestDTO, "transactionId");

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/trainings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(trainingRequestDTO)));
        resultActions.andExpect(status().isOk());
    }

    private String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}