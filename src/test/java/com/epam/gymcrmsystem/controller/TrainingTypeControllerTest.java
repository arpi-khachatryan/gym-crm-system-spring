package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.api.controller.TrainingTypeController;
import com.epam.gymcrmsystem.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystem.service.TrainingTypeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static com.epam.gymcrmsystem.parametrs.MockData.getTrainingTypeResponseDTO;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan on 04.06.2024
 */

public class TrainingTypeControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TrainingTypeService trainingTypeService;

    @InjectMocks
    private TrainingTypeController trainingTypeController;

    @BeforeEach
    void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(trainingTypeController).build();
        createTestDataSource();
    }

    private void createTestDataSource() throws SQLException {
        DataSource dataSource = new org.h2.jdbcx.JdbcDataSource();
        ((org.h2.jdbcx.JdbcDataSource) dataSource).setURL("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1");
        try (Connection connection = dataSource.getConnection(); Statement stmt = connection.createStatement()) {
            stmt.execute("DROP TABLE IF EXISTS trainer_trainee");
            stmt.execute("DROP TABLE IF EXISTS trainers");
            stmt.execute("DROP TABLE IF EXISTS trainees");
            stmt.execute("DROP TABLE IF EXISTS training_types");
            stmt.execute("DROP TABLE IF EXISTS trainings");
            stmt.execute("CREATE TABLE training_types (id INT AUTO_INCREMENT PRIMARY KEY, type_name VARCHAR(255) NOT NULL)");
        }
    }

    @AfterEach
    void tearDown() {
        reset(trainingTypeService);
    }

    @Test
    public void testGetTrainingTypes() throws Exception {
        TrainingTypeResponseDTO trainingTypeResponseDTO = getTrainingTypeResponseDTO();
        List<TrainingTypeResponseDTO> typeResponseDTO = List.of(trainingTypeResponseDTO);

        when(trainingTypeService.getTrainingTypes(any(String.class))).thenReturn(typeResponseDTO);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/trainingTypes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(typeResponseDTO)));
        resultActions.andExpect(status().isOk());
    }

    private String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}