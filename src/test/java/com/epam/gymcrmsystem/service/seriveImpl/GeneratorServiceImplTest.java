package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.repository.TraineeRepository;
import com.epam.gymcrmsystem.repository.TrainerRepository;
import com.epam.gymcrmsystem.service.serviceImpl.GeneratorServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.epam.gymcrmsystem.parametrs.MockData.getTrainee;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @author Arpi Khachatryan on 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class GeneratorServiceImplTest {

    @InjectMocks
    private GeneratorServiceImpl generatorService;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TraineeRepository traineeRepository;

    @Test
    public void testCalculateBaseUserName() {
        String firstName = getTrainee().getFirstName();
        String lastName = getTrainee().getLastName();
        String baseUserName = firstName + "." + lastName;
        assertEquals("firstName.lastName", baseUserName);
    }

    @Test
    public void testGenerateUniqueUserName() {
        String baseUserName = "firstName.lastName";
        int suffix = 1;
        when(traineeRepository.existsByUsername(baseUserName)).thenReturn(true);
        String uniqueUserName = generatorService.generateUniqueUserName(baseUserName);
        assertEquals(baseUserName + suffix, uniqueUserName);
    }

    @Test
    public void testGenerateRandomPassword() {
        String password = generatorService.generateRandomPassword();
        assertNotNull(password);
        assertEquals(10, password.length());
    }
}