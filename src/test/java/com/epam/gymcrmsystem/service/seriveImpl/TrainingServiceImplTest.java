package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.entity.Training;
import com.epam.gymcrmsystem.entity.TrainingType;
import com.epam.gymcrmsystem.exception.ResourceCreationException;
import com.epam.gymcrmsystem.repository.TraineeRepository;
import com.epam.gymcrmsystem.repository.TrainerRepository;
import com.epam.gymcrmsystem.repository.TrainingRepository;
import com.epam.gymcrmsystem.repository.TrainingTypeRepository;
import com.epam.gymcrmsystem.service.serviceImpl.TrainingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static com.epam.gymcrmsystem.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan on 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainingServiceImplTest {

    @InjectMocks
    private TrainingServiceImpl trainingService;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @Test
    public void testCreateTraining() {
        TrainingRequestDTO trainingRequestDTO = getTrainingRequestDTO();
        Trainee trainee = getTrainee();
        Trainer trainer = getTrainer();
        TrainingType trainingType = getTrainingType();

        when(traineeRepository.findByUsername(anyString())).thenReturn(Optional.of(trainee));
        when(trainerRepository.findByUsername(anyString())).thenReturn(Optional.of(trainer));
        when(trainingTypeRepository.findByName(anyString())).thenReturn(trainingType);
        when(trainingRepository.findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(anyString(), any(Date.class), anyInt(), anyString(), anyString(), anyString())).thenReturn(Optional.empty());
        when(trainingRepository.save(any(Training.class))).thenReturn(new Training());
        trainingService.createTraining(trainingRequestDTO, "transactionId");

        verify(traineeRepository, times(1)).findByUsername(trainingRequestDTO.getTraineeUsername());
        verify(trainerRepository, times(1)).findByUsername(trainingRequestDTO.getTrainerUsername());
        verify(trainingTypeRepository, times(1)).findByName(trainingRequestDTO.getTrainingTypeName());
        verify(trainingRepository, times(1)).findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(trainingRequestDTO.getTrainingName(), trainingRequestDTO.getTrainingDate(), trainingRequestDTO.getTrainingDuration(), trainingRequestDTO.getTraineeUsername(), trainingRequestDTO.getTrainerUsername(), trainingRequestDTO.getTrainingTypeName());
        verify(trainingRepository, times(1)).save(any(Training.class));
    }

    @Test
    public void testCreateTraining_TrainingAlreadyExists() {
        TrainingRequestDTO trainingRequestDTO = getTrainingRequestDTO();
        Trainee trainee = getTrainee();
        Trainer trainer = getTrainer();
        TrainingType trainingType = getTrainingType();
        Training existingTraining = getTraining();

        when(traineeRepository.findByUsername(anyString())).thenReturn(Optional.of(trainee));
        when(trainerRepository.findByUsername(anyString())).thenReturn(Optional.of(trainer));
        when(trainingTypeRepository.findByName(anyString())).thenReturn(trainingType);
        when(trainingRepository.findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(anyString(), any(Date.class), anyInt(), anyString(), anyString(), anyString())).thenReturn(Optional.of(existingTraining));
        assertThrows(ResourceCreationException.class, () -> trainingService.createTraining(trainingRequestDTO, "transactionId"));

        verify(traineeRepository, times(1)).findByUsername(trainingRequestDTO.getTraineeUsername());
        verify(trainerRepository, times(1)).findByUsername(trainingRequestDTO.getTrainerUsername());
        verify(trainingTypeRepository, times(1)).findByName(trainingRequestDTO.getTrainingTypeName());
        verify(trainingRepository, times(1)).findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(trainingRequestDTO.getTrainingName(), trainingRequestDTO.getTrainingDate(), trainingRequestDTO.getTrainingDuration(), trainingRequestDTO.getTraineeUsername(), trainingRequestDTO.getTrainerUsername(), trainingRequestDTO.getTrainingTypeName());
    }
}