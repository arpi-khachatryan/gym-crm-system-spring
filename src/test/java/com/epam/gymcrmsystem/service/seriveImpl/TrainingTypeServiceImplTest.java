package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystem.entity.TrainingType;
import com.epam.gymcrmsystem.exception.ResourceNotFoundException;
import com.epam.gymcrmsystem.mapper.TrainingTypeMapper;
import com.epam.gymcrmsystem.repository.TrainingTypeRepository;
import com.epam.gymcrmsystem.service.serviceImpl.TrainingTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.epam.gymcrmsystem.parametrs.MockData.getTrainingType;
import static com.epam.gymcrmsystem.parametrs.MockData.getTrainingTypeResponseDTO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan on 14.06.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainingTypeServiceImplTest {

    @InjectMocks
    private TrainingTypeServiceImpl trainingTypeService;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @Mock
    private TrainingTypeMapper trainingTypeMapper;

    @Test
    public void testGetTrainingTypes() {
        List<TrainingType> trainingTypes = List.of(getTrainingType());
        List<TrainingTypeResponseDTO> trainingTypeDTOs = List.of(getTrainingTypeResponseDTO());

        when(trainingTypeRepository.findAll()).thenReturn(trainingTypes);
        when(trainingTypeMapper.trainingTypeToTrainingTypeDTO(trainingTypes)).thenReturn(trainingTypeDTOs);
        List<TrainingTypeResponseDTO> result = trainingTypeService.getTrainingTypes("transactionId");

        verify(trainingTypeRepository, times(1)).findAll();
        verify(trainingTypeMapper, times(1)).trainingTypeToTrainingTypeDTO(trainingTypes);
        assertEquals(1, result.size());
        assertEquals("Fitness", result.get(0).getTrainingTypeName());
    }

    @Test
    public void testGetTrainingTypes_NotFound() {
        when(trainingTypeRepository.findAll()).thenReturn(new ArrayList<>());
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> trainingTypeService.getTrainingTypes("transactionId"));

        assertEquals("Error retrieving training types.", exception.getMessage());
        verify(trainingTypeRepository, times(1)).findAll();
        verify(trainingTypeMapper, never()).trainingTypeToTrainingTypeDTO(anyList());
    }
}